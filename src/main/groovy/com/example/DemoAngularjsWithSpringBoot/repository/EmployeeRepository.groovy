package com.example.DemoAngularjsWithSpringBoot.repository

import com.example.DemoAngularjsWithSpringBoot.entity.Employee

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository extends JpaRepository<Employee , String>{

}