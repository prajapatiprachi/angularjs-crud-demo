package com.example.DemoAngularjsWithSpringBoot.controller

import com.example.DemoAngularjsWithSpringBoot.co.EmployeeCO
import com.example.DemoAngularjsWithSpringBoot.dto.ResponseDTO
import com.example.DemoAngularjsWithSpringBoot.entity.Employee
import com.example.DemoAngularjsWithSpringBoot.repository.EmployeeRepository
import com.example.DemoAngularjsWithSpringBoot.service.EmployeeServiceImpl
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController


@RestController()
@CrossOrigin(origins = "http://localhost:4200")
class EmployeeController {

    @Autowired
    EmployeeServiceImpl employeeService

    @PostMapping(value = "/employee/save", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseDTO save(@RequestBody EmployeeCO employee) {
       return employeeService.save(employee)
    }

    @GetMapping("/employee/list")
    List<Employee> listAllUsers() {
        return employeeService.getAllEmployee()
    }

    @GetMapping("/employee/get/{id}")
    Employee getOne(@PathVariable("id") String id) {
        return  employeeService.findById(id)
    }

    @PutMapping(value = "/employee/updateEmployee")
    Employee update(@RequestBody EmployeeCO employeeCO) {
       return employeeService.update(employeeCO)
    }

    @DeleteMapping("/employee/deleteEmployee/{id}")
    List<Employee> deleteUser(@PathVariable("id") String id) {
        employeeService.deleteById(id)
        return employeeService.getAllEmployee()
    }

}
