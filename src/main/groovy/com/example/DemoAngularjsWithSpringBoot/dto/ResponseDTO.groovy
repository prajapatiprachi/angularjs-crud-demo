package com.example.DemoAngularjsWithSpringBoot.dto

class ResponseDTO<T> {

    boolean  status = true
    String  message = ''
    T data = data?: null

    void setSuccessResponse(T data, String msg = '') {
      this.status= true
        this.message = msg
        this.data = data
    }

    void setFailureResponse(String msg = '') {
        this.status= false
        this.message = msg
        this.data = null
    }

    void setErrorMessage(Exception e , String msg = '') {
        status= false
        this.message = msg?: e.message
        this.data = null
        e.printStackTrace()
    }
}
