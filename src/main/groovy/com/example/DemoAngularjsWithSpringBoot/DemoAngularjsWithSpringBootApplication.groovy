package com.example.DemoAngularjsWithSpringBoot

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class DemoAngularjsWithSpringBootApplication {

	static void main(String[] args) {
		SpringApplication.run(DemoAngularjsWithSpringBootApplication, args)
	}

}
