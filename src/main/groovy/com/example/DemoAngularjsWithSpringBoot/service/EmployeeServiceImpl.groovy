package com.example.DemoAngularjsWithSpringBoot.service

import com.example.DemoAngularjsWithSpringBoot.co.EmployeeCO
import com.example.DemoAngularjsWithSpringBoot.constants.MessageConstant
import com.example.DemoAngularjsWithSpringBoot.dto.ResponseDTO
import com.example.DemoAngularjsWithSpringBoot.entity.Employee
import com.example.DemoAngularjsWithSpringBoot.repository.EmployeeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository

    @Override
    ResponseDTO save(EmployeeCO employeeCO) {
        ResponseDTO responseDTO = new ResponseDTO()
        try {
            Employee employee = new Employee(
                    name: employeeCO?.name,
                    position: employeeCO?.position,
                    address: employeeCO?.address
            )
            employeeRepository.save(employee)
            responseDTO.setSuccessResponse("", MessageConstant.DATA_SAVE_SUCCESS)
        } catch (Exception e) {
            responseDTO.setErrorMessage(e)
        }
        return responseDTO
    }

    @Override
    List<Employee> getAllEmployee() {
        return employeeRepository.findAll()
    }

    @Override
    Employee findById(String id) {
        return employeeRepository.findById(id).get()
    }

    @Override
    Employee update(EmployeeCO employeeCO) {
        Employee employee = employeeRepository.findById(employeeCO.id).get()
        employee.name = employeeCO.name
        employee.position = employeeCO.position
        employee.address = employeeCO.address
        return employeeRepository.save(employee)

    }

    @Override
    void deleteById(String id) {
        employeeRepository.deleteById(id)
    }

}
